# README #

Django Web app API is hosted at [heroku](https://ft-labs07.herokuapp.com/tracker)

All the apis' enpoint have been tested in POSTMAN

Post request are done using form-data from Postman

### The following api endpoints are served ###

1.  '/' endpoint [GET]
    It shows all the users and corresponsing time they have spent in the site
2.  '/register' endpoint [POST]
    It register a user, using form data with fields as name, password and tz(timzone), the valis time zone are taken from [here](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)
3.  '/login' endpoint [POST]
    It logins a user with name and password as field via form-data
4.  '/startActivity/<int:id>' endpoint [GET]
    It starts a activity for the user with given Id
5.  '/stopActivity/<int:id>' endpoint [GET]
    It stops a activity for user with given Id

### How do set this project ###

* Make an Environemnt variable
* pip install -r requirements.txt
* pytho manage.py makemigrations
* pytho manage.py migrate
* pytho manage.py runserver
* Use the above endpoints

There is some already existing entries at endpoint https://ft-labs07.herokuapp.com/tracker/
