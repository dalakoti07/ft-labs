from django.db import models
from django.utils import timezone

# Create your models here.
class User(models.Model):
    real_name = models.CharField(max_length=20)
    tz= models.CharField(max_length=20)
    password = models.CharField(max_length=100)

    def __str__(self) :
        return self.real_name+" : "+self.tz

class Duration(models.Model):
    start_time = models.CharField(max_length=30,null=True, blank=True)
    end_time = models.CharField(max_length=30,null=True, blank=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE)


