from django.urls import path

from . import views

urlpatterns = [
path('', views.index, name='index'),
path('register/',views.register,name='register'),
path('login/',views.login,name='login'),
path('startActivity/<int:user_id>/',views.startActivity,name='start-activity'),
path('stopActivity/<int:user_id>/',views.stopActivity,name='stop-activity'),
]