from django.apps import AppConfig


class FtlabsConfig(AppConfig):
    name = 'FTLabs'
