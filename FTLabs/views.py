from django.shortcuts import render
from .models import User, Duration
from django.http import JsonResponse
from django.core import serializers
from django.forms.models import model_to_dict
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
import json
import pytz
from datetime import datetime
from django.utils import timezone
import hashlib
# Create your views here.

alltimeZones=pytz.all_timezones

# get all users with their activities detail
def index(request):
    response={"ok":True}
    # serialized_obj = serializers.serialize('json', [ obj, ])
    allUsers=[]
    for user in User.objects.all():
        timezone.activate(user.tz)
        userDict=model_to_dict( user )
        userDict.pop("password")
        print( userDict," and timmings are ")
        userActivitiesList=[]
        for durationObject in user.duration_set.all():
            durationJSONObject = model_to_dict(durationObject)
            durationJSONObject.pop('id')
            durationJSONObject.pop('user')
            durationJSONObject["start_time"]=durationObject.start_time#.strftime('%b %d %Y %H:%M %p')
            durationJSONObject["end_time"]="going on " if durationObject.end_time == None else durationObject.end_time#.strftime('%b %d %Y %H:%M %p') 
            userActivitiesList.append(durationJSONObject)
        userDict["activity_periods"]=userActivitiesList
        allUsers.append(userDict)
    response["members"]=allUsers
    return JsonResponse(response,safe=False)

@csrf_exempt 
@require_http_methods(["POST"])
def register(request):
    if request.method=='POST':
        name,password,tz =request.POST.get('name',''),request.POST.get('password',''),request.POST.get('tz','')
        # print(name,password,tz)
        if not tz in alltimeZones :
            return JsonResponse("invalid timezone ",safe =False)
        h = hashlib.md5(password.encode())
        password=h.hexdigest()
        user = User(real_name=name,tz=tz,password=password)
        user.save()
        userDict=model_to_dict( user )
        userDict.pop("password")
        return JsonResponse( userDict,safe=False)

@csrf_exempt 
@require_http_methods(["POST"])
def login(request):
    name,password =request.POST.get('name',''),request.POST.get('password','')
    h = hashlib.md5(password.encode())
    password=h.hexdigest()
    user= User.objects.all().filter(real_name=name).filter(password=password).first()
    if user==None:
        res={"status":"Error","message":"Wrong creds."}
        return JsonResponse(res,status=400)
    else:
        user=model_to_dict(user)
        user.pop("password")
        return JsonResponse(user)

def startActivity(request,user_id):
    # if one activity is already started then don't start new one 
    user= User.objects.all().filter(id=user_id).first()
    if user==None:
        return JsonResponse("invalid userId",safe=False)
    # check if a activity is already opened or not
    goingOnDuration= user.duration_set.all().filter(end_time=None)
    if goingOnDuration:
        result={"id":user_id,"status":"another activity is already going on, can't start a new session"}
        return JsonResponse(result,status=400) 
    startTimeAsPerTimeZone = datetime.now(pytz.timezone(user.tz))
    durationEntry=user.duration_set.create(start_time=startTimeAsPerTimeZone.strftime('%b %d %Y %H:%M %p'))
    durationEntry.save()
    print("activity started  ",model_to_dict(durationEntry))
    return JsonResponse(model_to_dict(durationEntry))

def stopActivity(request,user_id):
    # if one activity is already started then don't start new one 
    user= User.objects.all().filter(id=user_id).first()
    if user==None:
        return JsonResponse("invalid userId",safe=False)
    # check if a activity is already opened or not
    goingOnDuration= user.duration_set.all().filter(end_time=None).first()
    if goingOnDuration ==None:
        result={"id":user_id,"status":"no activity is going on, can't stop any activity"}
        return JsonResponse(result,status=400) 
    goingOnDuration.end_time=datetime.now(pytz.timezone(user.tz)).strftime('%b %d %Y %H:%M %p')
    goingOnDuration.save()
    return JsonResponse(model_to_dict(goingOnDuration))