from django.contrib import admin

# Register your models here.
from .models import User, Duration

admin.site.register([User,Duration])